﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace substitute
{
    class Program
    {
        string ReadTextFile(string file)
        {
            using (TextReader tr = File.OpenText(file))
            {
                return tr.ReadToEnd();
            }
        }

        static void Main(string[] args)
        {
            if (args.Length < 3)
            {
                Console.WriteLine("Parmeters input text file, input data file and output file missing!");
                return;
            }

            using (TextReader textReader = File.OpenText(args[0]))
            {
                using (TextReader dataReader = File.OpenText(args[1]))
                {
                    using (StreamWriter writer = new StreamWriter(args[2]))
                    {
                        var dataMap = new Dictionary<string, string>();
                        string data = dataReader.ReadToEnd();
                        var dataArr = data.Split('\n');
                        foreach (var dataEl in dataArr)
                        {
                            if (!string.IsNullOrWhiteSpace(dataEl))
                            {
                                var pair = dataEl.Split('=');
                                dataMap[pair[0].Trim()] = pair[1].Trim();
                            }
                        }

                        string line = null;
                        bool started = false;
                        StringBuilder tmp = new StringBuilder();
                        
                        while (null != (line = textReader.ReadLine()))
                        {
                            StringBuilder newLine = new StringBuilder();
                            foreach (var ch in line)
                            {
                                if (started)
                                {
                                    if (ch != '%')
                                        tmp.Append(ch);
                                    else
                                    {
                                        started = false;
                                        if (dataMap.ContainsKey(tmp.ToString()))
                                            newLine.Append(dataMap[tmp.ToString()]);
                                        tmp.Clear();
                                    }
                                }
                                else
                                {
                                    if (ch == '%')
                                        started = true;
                                    else
                                        newLine.Append(ch);
                                }
                            }
                            writer.WriteLine(newLine.ToString());
                        }
                    }
                }
            }
        }
    }
}
